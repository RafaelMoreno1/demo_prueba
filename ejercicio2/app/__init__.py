from flask import Flask
import redis

def create_app():
    app = Flask(__name__)
    app.redis = redis.Redis(host='localhost', port=6379, db=0)
    return app
